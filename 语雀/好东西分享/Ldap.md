# LDAP使用手册

### 一、LDAP简介
LDAP是轻量级目录访问协议的简称（Lightweight Directory Access Protocal）,用户访问目录服务。他是X500目录访问协议的一直，但是简化了实现方法
### 二、目录服务与关系数据库之间的区别

- 目录查询操作比关系数据库有更高的效率，但是更新效率比关系数据库低
- 目录不支持关系数据库那样的复杂查询，比如两个表的链接
- 目录不支持多操作的事务完整性，没有方式确认一些操作是全部成功还是全部失败
- 目录能够能好和更灵活的支持子查询和匹配查询
- 目录协议更适合应用于广域网，比如因特网或者大型公司的网络
- 目录的管理，配置，和调试比关系型数据库更简单
- 在使用关系数据库之前，必须首先定义表结构(模式)才可以进行操作。而目录中所使用的模式是由LDAP定义好的一系列类组成的。对于目录中的每条记录中必须属于其中的一个类或者多个类。这些类定义了该记录中可以存储的信息。
- 目录以对象的形式存储数据。信息被组织成树型结构。
- 目录服务支持分布式存储结构，容易实现数据的扩展，能满足大容量存储的要求。

### 三、LDAP的优点
- 可以存储在其它条件下很难存储的管理信息
- 数据安全可靠，访问控制粒度细腻
- LDAP是一个标准的，开放的协议，具有平台无关性
- 数据分布广，规模可灵活扩充
- LDAP目录服务器可以使任何一种开放源代码或商用的LDAP目录服务器

### **四、LDAP模型** ###
> LDAP模型是从X500协议中继承过来的。是LDAP的一个组成部分，用于指导客户如何使用目录服务
LDAP定义了四个模型，包括信息模型，命名模型，功能模型，安全模型。

- LDAP信息模型
    LDAP信息模型用于描述LDAP中信息的表达方式。
    LDAP信息模型包含三部分：Entries Attributes Values。
    * Entry:Directory 中最基本的信息单元，Entry中所包含的信息描述了显示世界中的一个真是的对象，在目录系统中它可以理解为目录树中的一个节点。在目录中添加一个Entry时，该Entry必须属于一个或多个object class ,每一个object class 规定了该Entry中必须要包含的属性，以及允许使用的属性。Entry所属的类型由属性objectclass规定。每一个Entry都有一个DN(distinguished name) 用于唯一的标志Entry在directory中的位置。如下图所示：
    ```flow    
        st=>operation: The Organization
        e=>end: End:>http://www.baidu.com
        
        op1=>operation: My Operation|past
        op2=>operation: Stuff|current
        sub1=>subroutine: My Subroutine|invalid
        cond=>condition: Yes or No?|approved:>http://www.baidu.com
        c2=>condition: Good idea|rejected
        io=>inputoutput: catch something...|request

        st->op1(right)->cond
        cond(yes, right)->c2
        cond(no)->sub1(left)->op1
        c2(yes)->io->e
        c2(no)->op2->e
    ```


    ```flow    
        st=>start: Start|past:>http://www.baidu.com
        e=>end: End:>http://www.baidu.com
        op1=>operation: My Operation|past
        op2=>operation: Stuff|current
        sub1=>subroutine: My Subroutine|invalid
        cond=>condition: Yes or No?|approved:>http://www.baidu.com
        c2=>condition: Good idea|rejected
        io=>inputoutput: catch something...|request

        st->op1(right)->cond
        cond(yes, right)->c2
        cond(no)->sub1(left)->op1
        c2(yes)->io->e
        c2(no)->op2->e
    ```

    ```mermaid
    graph TD;
    id[The organization itself]--dc=example,dc=com-->id1(( ));
    id1-->id1_1(( ));
    id1-->id1_2(( ));
    id1_1-->id1_1_1(( ))
    id1_1-->id1_1_2(( ))
    id1_2-->id1_2_1(( ))
    id1_2-->id1_2_2(( ));
    OU1[The orgnization unit]-.->id1_1;
    OU1[The orgnization unit]-.->id1_2;
    
    ```
